# yarn-why-json

> A machine readable `yarn why` which returns the results as json

This module provides a function and a CLI to answer the questions:
Why do I have a certain dependency in my dependency graph?
What paths do lead to that dependency?

It makes use of yarn's own package file parser, detects cycles and returns all paths to a dependency.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
  - [Examples](#examples)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```
yarn global add yarn-why-json
```

## Usage

```
Usage: yarn-why-json [options] <package> [version]

Options:
  -V, --version           output the version number
  -l, --lock-file [path]  Path to `yarn.lock` file. Defaults to current working directory (default: "yarn.lock")
  -h, --help              output usage information
```

### Examples

- Searching for all versions of a package: `yarn-why-json promise-polyfill`:
  ```
  [
    [
      {
        "key": "child-process-promise@^2.2.1",
        "name": "child-process-promise",
        "requestedVersion": "^2.2.1",
        "version": "2.2.1",
        "resolved": "https://registry.yarnpkg.com/child-process-promise/-/child-process-promise-2.2.1.tgz#4730a11ef610fad450b8f223c79d31d7bdad8074",
        "integrity": "sha1-RzChHvYQ+tRQuPIjx50x172tgHQ="
      },
      {
        "key": "promise-polyfill@^6.0.1",
        "name": "promise-polyfill",
        "requestedVersion": "^6.0.1",
        "version": "6.1.0",
        "resolved": "https://registry.yarnpkg.com/promise-polyfill/-/promise-polyfill-6.1.0.tgz#dfa96943ea9c121fca4de9b5868cb39d3472e057",
        "integrity": "sha1-36lpQ+qcEh/KTem1hoyznTRy4Fc="
      }
    ]
  ]
  ```
- Searching for a specific version of a package: `yarn-why-json commander 2.19.0`:
  ```json
  [
    [
      {
        "key": "commander@^2.19.0",
        "name": "commander",
        "requestedVersion": "^2.19.0",
        "version": "2.19.0",
        "resolved": "https://registry.yarnpkg.com/commander/-/commander-2.19.0.tgz#f6198aa84e5b83c46054b94ddedbfed5ee9ff12a",
        "integrity": "sha512-6tvAOO+D6OENvRAh524Dh9jcfKTYDQAqvqezbCW82xj5X0pSrcpxtvRKHLG0yBY6SD7PSDrJaj+0AiOcKVd1Xg=="
      }
    ]
  ]
  ```

## Maintainers

[@leipert](https://gitlab.com/leipert)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2018 Lukas Eipert
