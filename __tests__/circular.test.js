const { renderer } = require("./helper");
const fs = require("fs");
const lockfile = require("@yarnpkg/lockfile");

const whyJSON = require("../lib/why-json");

const circular = lockfile.parse(
  fs.readFileSync(__dirname + "/__fixtures__/cycle/yarn.lock", "utf8")
);

describe("circular yarn.lock,", () => {
  it("finds a properly", () => {
    const result = whyJSON(circular.object, "a");
    expect(renderer(result)).toMatchSnapshot();
  });

  it("finds b properly", () => {
    const result = whyJSON(circular.object, "b");
    expect(renderer(result)).toMatchSnapshot();
  });
});
