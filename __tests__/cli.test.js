const fs = require("fs");
const path = require("path");
const renderer = require("./helper").renderer;
const cpExec = require("child-process-promise").exec;

const exec = command => {
  return cpExec(command, { maxBuffer: 400 * 1024 });
};

const baseDir = path.join(__dirname, "..");

const program = path.join(baseDir, "lib", "cli.js");

describe("yarn-why-json cli error cases", () => {
  it("throws error if searchedPackage argument is missing", () => {
    return exec(program).catch(error => {
      expect(error.code).toEqual(1);
      expect(error.message).toMatch(/You need to provide a package name/);
    });
  });

  it("throws error if yarn.lock does not exist", () => {
    return exec(`${program} --lock-file nonExistingLock.file debug`).catch(
      error => {
        expect(error.code).toEqual(1);
        expect(error.message).toMatch(/ENOENT: no such file or directory/);
      }
    );
  });

  it("throws error if yarn.lock is not parsable", () => {
    const faultyYarnLock = path.join(
      baseDir,
      "__tests__",
      "__fixtures__",
      "corrupt.yarn.lock"
    );

    return exec(`${program} --lock-file ${faultyYarnLock} debug`).catch(
      error => {
        expect(error.code).toEqual(1);
        expect(error.message).toMatch(/Invalid value type 4:0 in lockfile/);
      }
    );
  });
});

describe("yarn-why-json cli proper cases", () => {
  it("can parse empty lock files", () => {
    const lockFile = path.join(
      baseDir,
      "__tests__",
      "__fixtures__",
      "empty.yarn.lock"
    );

    return exec(`${program} --lock-file ${lockFile} debug`).then(result => {
      expect(result.stderr).toEqual("");
      expect(result.stdout).toEqual("[]");
    });
  });

  it("returns results if the package is found", () => {
    const lockFile = path.join(
      baseDir,
      "__tests__",
      "__fixtures__",
      "unit",
      "yarn.lock"
    );

    return exec(`${program} --lock-file ${lockFile} ansi`).then(result => {
      expect(result.stderr).toEqual("");
      const parsed = JSON.parse(result.stdout);
      expect(renderer(parsed)).toEqual([["ansi@^0.3.1"]]);
    });
  });
});
