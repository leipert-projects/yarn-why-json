const { renderer } = require("./helper");

const fs = require("fs");
const lockfile = require("@yarnpkg/lockfile");

const whyJSON = require("../lib/why-json");

const json = lockfile.parse(
  fs.readFileSync(__dirname + "/__fixtures__/complex/yarn.lock", "utf8")
);

describe("complex yarn.lock", () => {
  it("finds anymatch properly", () => {
    const result = whyJSON(json.object, "anymatch");
    expect(renderer(result)).toMatchSnapshot();
  });

  it("finds micromatch properly", () => {
    const result = whyJSON(json.object, "micromatch");
    expect(renderer(result)).toMatchSnapshot();
  });

  it("finds debug@2.6.8 properly", () => {
    const result = whyJSON(json.object, "debug", "2.6.8");
    expect(renderer(result)).toMatchSnapshot();
  });
});
