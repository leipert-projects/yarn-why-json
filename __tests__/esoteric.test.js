const fs = require("fs");
const lockfile = require("@yarnpkg/lockfile");

const whyJSON = require("../lib/why-json");

const json = lockfile.parse(
  fs.readFileSync(__dirname + "/__fixtures__/esoteric/yarn.lock", "utf8")
);

describe("esoteric yarn.lock", () => {
  it("finds abbrev with `*`", () => {
    const result = whyJSON(json.object, "abbrev");
    expect(result).toMatchSnapshot();
  });

  it("finds ansi-colors with URL", () => {
    const result = whyJSON(json.object, "ansi-colors");
    expect(result).toMatchSnapshot();
  });

  it("finds ansi-html with git URL", () => {
    const result = whyJSON(json.object, "ansi-html");
    expect(result).toMatchSnapshot();
  });

  it("finds atob without version", () => {
    const result = whyJSON(json.object, "atob");
    expect(result).toMatchSnapshot();
    expect(result[0][1].requestedVersion).toBe("");
  });

  it("finds deep with file path", () => {
    const result = whyJSON(json.object, "deep");
    expect(result).toMatchSnapshot();
  });

  it("finds lodash with version range", () => {
    const result = whyJSON(json.object, "lodash");
    expect(result).toMatchSnapshot();
  });

  it("finds moment with github shortcut", () => {
    const result = whyJSON(json.object, "moment");
    expect(result).toMatchSnapshot();
  });
});
