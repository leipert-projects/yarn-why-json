module.exports = function(yarn, pkg, version = null) {
  const inverseTree = {};
  const targets = [];

  Object.keys(yarn).forEach(key => {
    const details = yarn[key];

    if (!inverseTree[key]) {
      inverseTree[key] = { inverseDependencies: [] };
    }

    Object.assign(inverseTree[key], details);

    if (key.startsWith(`${pkg}@`)) {
      if (version === null || details.version === version) {
        targets.push(key);
      }
    }

    const deps = Object.assign(
      {},
      details.dependencies || {},
      details.optionalDependencies || {}
    );

    Object.keys(deps).forEach(pkg => {
      const semver = deps[pkg];
      const fullKey = `${pkg}@${semver}`;
      if (!inverseTree[fullKey]) {
        inverseTree[fullKey] = { inverseDependencies: [] };
      }

      inverseTree[fullKey].inverseDependencies.push(key);
    });
  });

  const result = [];

  while (targets.length > 0) {
    let curr = targets.pop();
    const currPath = inverseTree[curr].path || [];

    do {
      currPath.push(curr);

      const parents = [...inverseTree[curr].inverseDependencies];

      curr = parents.pop();
      if (currPath.includes(curr)) {
        curr = null;
      }

      if (parents.length > 0) {
        parents.forEach(key => {
          if (!targets.includes(key) && !currPath.includes(key)) {
            inverseTree[key].path = [...currPath];
            targets.push(key);
          }
        });
      }
    } while (curr);

    result.push(currPath.reverse());
  }

  return result
    .sort((a, b) => {
      if (a.length < b.length) {
        return -1;
      }
      if (a.length > b.length) {
        return 1;
      }

      for (let i = 0; i < a.length; i++) {
        if (a[i] < b[i]) {
          return -1;
        }

        if (a[i] > b[i]) {
          return 1;
        }
      }
      return 0;
    })
    .map(x =>
      x.map(key => {
        const [name, requestedVersion] = key.split("@", 2);
        const { version, resolved, integrity } = yarn[key];
        const result = { key, name, requestedVersion, version };
        if (resolved !== undefined) {
          result.resolved = resolved;
        }
        if (integrity !== undefined) {
          result.integrity = integrity;
        }
        return result;
      })
    );
};
